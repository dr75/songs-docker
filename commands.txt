Build Docker image locally:
~/repos/songs-docker$ sudo docker build -t songs docker

Publish to Docker Hub:
~/repos/songs-docker$ sudo docker push appenzellerd/songs
