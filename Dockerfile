FROM strm/latex:latest

# add support for Unicode
RUN apt-get update && apt-get install -y locales locales-all
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

# get C compiler
RUN apt-get update && \
    apt-get -y install gcc

# install Songs Latex Package
RUN mkdir -p /usr/src/songs && \
    cd /usr/src/songs && \
    curl -L http://downloads.sourceforge.net/songs/songs-2.18.tar.gz | tar -xz
WORKDIR /usr/src/songs/songs-2.18
RUN ln -s /usr/bin/make /usr/bin/gmake # see: https://ubuntuforums.org/showthread.php?t=389841
RUN ./configure && \
    gmake

# process input book
RUN cp -r sample book && \
    cd book && \
    gmake clean && \
    rm *.tex && \
    rm *.sbd
ENTRYPOINT cp /book/* book/ && \
           cd book && \
           gmake && \
           cp -v *.pdf /book
